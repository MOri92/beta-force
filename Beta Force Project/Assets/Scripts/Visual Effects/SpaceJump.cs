﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SpaceJump : MonoBehaviour
{
    [SerializeField] private VisualEffect vfx = null;

    private const string rate = "Rate";
    private const string speed = "Speed";

    public IEnumerator BeginSpaceJump() 
    {
        float counter = 0;

        vfx.SetFloat(rate, 50);
        vfx.SetFloat(speed, 1);

        while (counter < 1)
        {
            counter += Time.deltaTime * 2;
            vfx.SetFloat(rate, Mathf.Lerp(50, 100, counter));
            vfx.SetFloat(speed, Mathf.Lerp(1, 3, counter));
            yield return null;
        }
    }

    public IEnumerator EndSpaceJump()
    {
        float counter = 0;

        vfx.SetFloat(rate, 100);
        vfx.SetFloat(speed, 3);

        while (counter < 1)
        {
            counter += Time.deltaTime * 2;
            vfx.SetFloat(rate, Mathf.Lerp(100, 0, counter));
            vfx.SetFloat(speed, Mathf.Lerp(3, 1, counter));
            yield return null;
        }
    }
}
