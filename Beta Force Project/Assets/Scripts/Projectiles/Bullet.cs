﻿using System.Collections;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public int actualDamage = 1;

    [SerializeField] private int bulletDamage = 1;
    [SerializeField] private float bulletSpeed = 1;

    private Vector3 initialScale = Vector3.zero;

    private Rigidbody2D rg = null;
    private Ship_Cannon cannon = null;

    private void Awake()
    {
        rg = GetComponent<Rigidbody2D>();
        cannon = transform.parent.GetComponent<Ship_Cannon>();
    }

    private void Start()
    {
        initialScale = transform.localScale;
    }

    public void ResetProjectile()
    {
        gameObject.SetActive(false);
        cannon.projectiles.Add(this);
        transform.parent = cannon.transform;
        transform.position = cannon.transform.position;
        transform.localRotation = Quaternion.identity;
        transform.localScale = initialScale;
    }

    public void Shot(int damageMod, float speedMod)
    {
        actualDamage = bulletDamage + damageMod;
        transform.parent = null;
        rg.AddForce(cannon.transform.up * bulletSpeed * speedMod);
        StartCoroutine(ReturnToShip());
    }

    private IEnumerator ReturnToShip()
    {
        yield return new WaitForSeconds(5f);
        ResetProjectile();
    }    
}
