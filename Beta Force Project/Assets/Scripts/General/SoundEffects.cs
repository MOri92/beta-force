﻿using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    [SerializeField] private AudioClip[] clips = new AudioClip[5];
    [SerializeField] private AudioSource sound = null;

    private void Awake()
    {
        Global.sounds = this;
    }

    public void PlayerShoots() 
    {
        sound.PlayOneShot(clips[0]);
    }
    
    public void EnemyShoots() 
    {
        sound.PlayOneShot(clips[1]);
    }

    public void ShieldUp()
    {
        sound.PlayOneShot(clips[2]);
    }

    public void ShipDestroyed()
    {
        sound.PlayOneShot(clips[3]);
    }

    public void BeginSpaceJump()
    {
        sound.PlayOneShot(clips[4]);
    }

    public void EndSpaceJump() 
    {
        sound.clip = clips[5];
        sound.time = 2f;
        sound.Play();
    }
    
    public void PlayerHitted() 
    {
        sound.PlayOneShot(clips[6]);
    } 
    
    public void PlayerDodge() 
    {
        sound.PlayOneShot(clips[7]);
    }
}
