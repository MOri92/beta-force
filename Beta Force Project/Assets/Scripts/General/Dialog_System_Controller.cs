﻿using TMPro;
using UnityEngine;
using System.Collections;

public class Dialog_System_Controller : MonoBehaviour
{
    public D_Void onFinished = null;

    [SerializeField] private GameObject dialogPanel = null;

    [SerializeField] private TMP_Text dialog = null;

    [SerializeField] private float textSpeed = 0.01f;

    public int currentLine = 0;

    private Dialog_System_Database database = null;


    // Start is called before the first frame update
    void Start()
    {
        database = GetComponent<Dialog_System_Database>();
    }

    public void NewDialog(int toSay, D_Void finish)
    {
        if (onFinished == null) onFinished += finish;

        currentLine = toSay;
        dialogPanel.SetActive(true);
       
        StartCoroutine(LetterByLetter(database.dialogs[currentLine].line));
    }

    private IEnumerator LetterByLetter(string toSay)
    {
        dialog.text = "";

        for (int i = 0; i < toSay.Length; i++)
        {
            dialog.text += "<alpha=#00>" + toSay[i];
        }

        for (int i = 0; i < toSay.Length; i++)
        {
            if (Input.GetMouseButton(0)) break;
            dialog.text = dialog.text.Remove(i, 11);
            yield return new WaitForSeconds(textSpeed);
        }

        dialog.text = toSay;
        StartCoroutine(NextDialog());
    }

    private IEnumerator NextDialog()
    {
        yield return new WaitForSeconds(0.5f);

        while (!Input.GetMouseButtonUp(0))
        {
            yield return null;
        }

        if (!database.dialogs[currentLine + 1].empty)
        {            
            NewDialog(currentLine + 1, null);
        }
        else
        {
            EndConversation();
        }
    }

    private void EndConversation() 
    {
        dialogPanel.SetActive(false);
        onFinished();
        onFinished = null;
    }
}
