﻿using UnityEngine;

public class Dialog_System_Database : MonoBehaviour
{
    public Dialog[] dialogs = new Dialog[20];
}

[System.Serializable]
public class Dialog 
{
    public string line = "";
    public bool empty = false;
}
