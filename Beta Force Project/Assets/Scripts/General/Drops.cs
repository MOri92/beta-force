﻿using System.Collections;
using UnityEngine;

public abstract class Drops : MonoBehaviour
{
    [SerializeField] private float fallSpeed = 0.05f;

    private Coroutine falling = null;

    public void Fall() 
    {
        transform.parent = null;
        gameObject.SetActive(true);
        falling = StartCoroutine(Falling());    
    }

    protected IEnumerator Falling() 
    {
        float counter = 0;

        Vector3 start = transform.position;
        Vector3 end = new Vector3(0, -Global.screenHeight - 10, 1);

        while (counter < 1)
        {
            counter += Time.deltaTime * fallSpeed;
            transform.position = Vector3.Lerp(start, end, counter);
            yield return null;
        }

        Disappear();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) 
        {
            StopCoroutine(falling);
            gameObject.SetActive(false);
            GetDrop();
        }
    }

    protected virtual void GetDrop() 
    {

    }

    protected void Disappear() 
    {
        Destroy(gameObject);
    }
}
