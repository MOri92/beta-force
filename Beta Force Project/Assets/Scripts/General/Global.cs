﻿public static class Global 
{
    public static int currentLevel = 1;

    public static int money = 0;

    public static float screenWidth = 0.0f;
    public static float screenHeight = 0.0f;

    public static int[] shipLevels = new int[6];

    public static bool playerCanMove = false;
    public static bool playerGotShield = false;

    public static Ship_Stats stats = null;
    public static SoundEffects sounds = null;
    public static Ship_Controller ship = null;
    public static Level_Controller levelController = null;
}
