﻿using System.Collections;
using UnityEngine;

public class Formation_CircleAround : Enemy_Formation
{
    [SerializeField] private float spawnSpeed = 0.5f;
    [SerializeField] private float radius = 50.0f;

    private int shipsReady = 0;
    private bool rotate = false;

    private void Update()
    {
        if(rotate == true) 
        {
            transform.Rotate(new Vector3(0, 0, Time.deltaTime * 10));
        }
    }

    protected override void SpawnSquad()
    {
        Vector3 startingPoint = GetStartingPosition();
        Vector3[] endPoint = GetEndPoint();

        StartCoroutine(MoveSquad(startingPoint, endPoint));
    }

    private Vector3 GetStartingPosition()
    {
        Vector3 start = new Vector3(Random.Range(-Global.screenWidth + 5, Global.screenWidth - 5), Global.screenHeight + 5, 1);
        return start;
    }

    private Vector3[] GetEndPoint()
    {
        Vector3[] positions = new Vector3[enemies.Count];

        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] = Quaternion.Euler(0, (i + 1) * 360 / enemies.Count, 0) * Vector3.forward * radius;
            positions[i] = new Vector3(positions[i].x, positions[i].z, 1);           
        }

        return positions;
    }

    private void OnMovementFinished() 
    {
        shipsReady++;

        if(shipsReady == enemies.Count) rotate = true;
    }

    private IEnumerator MoveSquad(Vector3 start, Vector3[] ends)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].transform.position = start;
            Vector3 endAdjusted = new Vector3(ends[i].x, ends[i].y, 1);

            endAdjusted = new Vector3(Mathf.Clamp(endAdjusted.x, -Global.screenWidth + 10, Global.screenWidth - 10),
                                      Mathf.Clamp(endAdjusted.y, -Global.screenHeight + 10, Global.screenHeight - 10), 1);

            enemies[i].MoveTo(endAdjusted, OnMovementFinished);
            yield return new WaitForSeconds(spawnSpeed);
        }
    }
}
