﻿using System.Collections;
using UnityEngine;

public class Formation_Horizontal : Enemy_Formation
{
    [SerializeField] private float spawnSpeed = 0.5f;

    protected override void SpawnSquad()
    {
        Vector3 startingPoint = GetStartingPosition();
        Vector3 endPoint = GetEndPoint();

        StartCoroutine(MoveSquad(startingPoint, endPoint));
    }

    private Vector3 GetStartingPosition()
    {
        Vector3 start = new Vector3(-Global.screenWidth, Global.screenHeight + 5, 1);
        return start;
    }

    private Vector3 GetEndPoint()
    {
        Vector3 end = new Vector3(0, Random.Range(-Global.screenHeight, Global.screenHeight), 1);
        return end;
    }

    private IEnumerator MoveSquad(Vector3 start, Vector3 end)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].transform.position = new Vector3(Mathf.Clamp(start.x + Global.screenWidth * 2 * (i + 1) / enemies.Count,
                                           -Global.screenWidth + 5, Global.screenWidth - 5), start.y, start.z); 

            Vector3 endAdjusted = new Vector3(enemies[i].transform.position.x, end.y, end.z);

            endAdjusted = new Vector3(Mathf.Clamp(endAdjusted.x, -Global.screenWidth + 5, Global.screenWidth - 5), 
                                      Mathf.Clamp(endAdjusted.y, -Global.screenHeight + 5, Global.screenHeight - 5), 0);
            
            enemies[i].MoveTo(endAdjusted, null);
            yield return new WaitForSeconds(spawnSpeed);
        }
    }
}
