﻿using UnityEngine;

public class Drop_Health : Drops
{
    private int healEffect = 3;    

    protected override void GetDrop()
    {
        Global.stats.Heal(healEffect);
        Disappear();
    }
}
