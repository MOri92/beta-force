﻿using UnityEngine;

public class Drop_Gold : Drops
{
    private int amount = 1;

    protected override void GetDrop()
    {
        amount = Random.Range(8, 21);
        Global.ship.shipUI.GotMoney(amount);
        Disappear();
    }
}
