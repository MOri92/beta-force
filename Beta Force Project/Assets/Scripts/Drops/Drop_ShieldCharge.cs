﻿using UnityEngine;

public class Drop_ShieldCharge : Drops
{
    [SerializeField] private float shieldCharge = 0.2f;

    protected override void GetDrop()
    {
        Global.stats.RaiseShieldCharge(shieldCharge);
        Disappear();
    }
}
