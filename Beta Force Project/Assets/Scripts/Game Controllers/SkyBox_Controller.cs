﻿using UnityEngine;

public class SkyBox_Controller : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 0.01f;
    [SerializeField] private Material skybox = null;

    private float rotationAngle = 0.0f;

    void Update()
    {
        if (rotationAngle == 360) rotationAngle = 0;

        skybox.SetFloat("_Rotation", rotationAngle);
        rotationAngle += Time.deltaTime * rotationSpeed;
    }
}
