﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreen_Controller : MonoBehaviour
{
    [SerializeField] private Button start = null;
    [SerializeField] private Fader_Controller fader = null;
    [SerializeField] private List<CanvasGroup> cats = new List<CanvasGroup>();

    void Start()
    {
        StartCoroutine(Presentation());
    }

    private IEnumerator Presentation() 
    {
        yield return new WaitForSeconds(2.5f);

        for (int i = 0; i < cats.Count; i++)
        {
            yield return StartCoroutine(AppearCat(cats[i]));
            yield return new WaitForSeconds(0.5f);
        }

        yield return StartCoroutine(fader.FadeIn());
        start.interactable = true;
    }

    private IEnumerator AppearCat(CanvasGroup catToAppear) 
    {
        float counter = 0.0f;

        while (counter < 1.0f)
        {
            counter += Time.deltaTime;
            catToAppear.alpha = counter;
            yield return null;
        }
    }

    public void StartGameButtonPressed() 
    {
        fader.FadeEverything();
        StartCoroutine(StartingGame());
    }

    private IEnumerator StartingGame()
    {
        yield return StartCoroutine(fader.FadeOut());
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadSceneAsync(1);
    }
}
