﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Controller : MonoBehaviour
{
    [SerializeField] private Fader_Controller fader = null;
    [SerializeField] private Dialog_System_Controller dialog = null;

    [SerializeField] private AudioSource music = null;

    void Start()
    {
        Global.screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).x;
        Global.screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).y;

        if (Global.currentLevel == 1) 
        {
            SceneManager.LoadSceneAsync(2);
        }
        else if(Global.currentLevel == 2)
        {
            StartCoroutine(FirstTimeAtWorkshop());
        }
        else 
        {
            Global.ship.gameObject.SetActive(false);
            StartCoroutine(fader.FadeIn());
            music.Play();
        }
    }

    public void NextMission() 
    {
        if(Global.currentLevel == SceneManager.sceneCountInBuildSettings - 1) 
        {
            dialog.NewDialog(13, null);
        }

        StartCoroutine(ReadyingNextMission());
    }

    private IEnumerator ReadyingNextMission() 
    {
        yield return StartCoroutine(fader.FadeOut());
        SceneManager.LoadSceneAsync(Global.currentLevel + 1);
    }

    private IEnumerator FirstTimeAtWorkshop() 
    {
        Global.ship.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        dialog.NewDialog(0, ConversationOver);
    }

    private void ConversationOver() 
    {
        StartCoroutine(ReadyingToShop());
    }

    private IEnumerator ReadyingToShop() 
    {
        music.Play();
        yield return StartCoroutine(fader.FadeIn());
    }
}
