﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial_Controller : MonoBehaviour
{
    [SerializeField] private SpaceJump spaceJump = null;
    [SerializeField] private Level_Controller level = null;
    [SerializeField] private Fader_Controller fader = null;
    [SerializeField] private Dialog_System_Controller dialog = null;

    private void Start()
    {
        Global.ship.gameObject.SetActive(true);
        StartCoroutine(BeginTutorial());        
    }
    
    private IEnumerator BeginTutorial() 
    {
        yield return new WaitForSeconds(2.0f);
        yield return StartCoroutine(fader.FadeIn());
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(spaceJump.EndSpaceJump());
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(Global.ship.PlayerArriveAtLevel());
        
        spaceJump.gameObject.SetActive(false);

        yield return new WaitForSeconds(1.0f);
        dialog.NewDialog(0, StartMission);
    }

    private void StartMission() 
    {
        Global.playerCanMove = true;
        level.enabled = true;
        level.EndLevel += MissionComplete;
    }

    private void MissionComplete() 
    {
        Global.playerCanMove = false;
        StartCoroutine(ExitingMission());
    }

    private IEnumerator ExitingMission() 
    {
        yield return new WaitForSeconds(2.0f);
        dialog.NewDialog(8, StarJump);
    }

    private void StarJump() 
    {
        StartCoroutine(StarJumping());
    }

    private IEnumerator StarJumping() 
    {
        Global.playerGotShield = true;

        yield return new WaitForSeconds(1.0f);
        spaceJump.gameObject.SetActive(true);
        StartCoroutine(spaceJump.BeginSpaceJump());
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(Global.ship.PlayerLeaveLevel());
        yield return StartCoroutine(fader.FadeOut());
        yield return new WaitForSeconds(2f);
        Global.currentLevel++;
        SceneManager.LoadSceneAsync(1);
    }
}
