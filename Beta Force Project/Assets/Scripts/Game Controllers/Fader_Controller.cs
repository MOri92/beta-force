﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader_Controller : MonoBehaviour
{
    [SerializeField] private float speed = 0.8f;
    [SerializeField] private Image fade = null;

    public IEnumerator FadeIn()
    {
        float counter = 1.0f;

        while (counter > 0.0f)
        {
            counter -= Time.deltaTime * speed;
            fade.color = new Color(0, 0, 0, counter);
            yield return null;
        }
    }
    public IEnumerator FadeOut() 
    {
        float counter = 0.0f;

        while(counter < 1.0f) 
        {
            counter += Time.deltaTime * speed;
            fade.color = new Color(0, 0, 0, counter);
            yield return null;
        }
    }

    public void FadeEverything() 
    {
        fade.transform.SetAsLastSibling();
    }
}
