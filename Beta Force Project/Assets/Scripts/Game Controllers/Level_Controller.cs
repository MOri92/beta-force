﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level_Controller : MonoBehaviour
{
    public D_Void EndLevel;

    [SerializeField] private SpaceJump spaceJump = null;
    [SerializeField] private Fader_Controller fader = null;
    [SerializeField] private Enemy_Waves[] waves = new Enemy_Waves[5];

    #region LevelUI

    [SerializeField] private TMP_Text life = null;
    [SerializeField] private TMP_Text gold = null;
    [SerializeField] private Image shieldIcon = null;

    #endregion

    private int squadsInAction = 0;
    private int currentWave = 0;
    private int currentSubWave = 0;

    private void Awake()
    {
        Global.ship.transform.position = new Vector3(0, -Global.screenHeight - 10, 1);
        Global.ship.gameObject.SetActive(true);

        Global.levelController = this;
        Global.stats.hitPoints = Global.stats.maxHitPoints;
        Global.ship.shipUI.GetUIElements(life, gold, shieldIcon);
    }

    private void Start()
    {
        if (Global.currentLevel > 1)
        {
            EndLevel += EndMission;
            StartCoroutine(ReadyingPlayer());
        }
    }

    private void OnEnable()
    {
        StartWave();
    }

    private IEnumerator ReadyingPlayer()
    { 
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(fader.FadeIn());
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(spaceJump.EndSpaceJump());
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(Global.ship.PlayerArriveAtLevel());

        spaceJump.gameObject.SetActive(false);

        yield return new WaitForSeconds(1.0f);
        Global.playerCanMove = true;
        StartWave();
    }

    public void StartWave()
    {
        DeployNewSubWave();
    }

    public void isWaveOver()
    {
        squadsInAction--;

        if(squadsInAction == 0 && currentSubWave < waves[currentWave].subWaves.Length - 1) 
        {
            currentSubWave++;
            StartCoroutine(WaitForNewWave());
        }
        else if(squadsInAction == 0 && currentWave < waves.Length -1) 
        {
            currentWave++;
            currentSubWave = 0;
            StartCoroutine(WaitForNewWave());
        }
        else 
        {
            EndLevel();
            EndLevel = null;
        }
    }

    private IEnumerator WaitForNewWave() 
    {
        yield return new WaitForSeconds(3.0f);
        StartWave();
    }

    private void DeployNewSubWave()
    {
        Enemy_SubWave subwave = waves[currentWave].subWaves[currentSubWave];
        squadsInAction = subwave.enemySquads.Count;

        for (int i = 0; i < squadsInAction; i++)
        {
            subwave.enemySquads[i].DeploySquad(this, subwave.deployTime[i]);
        }        
    }

    private void EndMission() 
    {
        StartCoroutine(StarJumping());
    }

    private IEnumerator StarJumping()
    {
        Global.playerCanMove = false;
        yield return new WaitForSeconds(1.0f);
        spaceJump.gameObject.SetActive(true);
        StartCoroutine(spaceJump.BeginSpaceJump());
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(Global.ship.PlayerLeaveLevel());
        yield return StartCoroutine(fader.FadeOut());
        yield return new WaitForSeconds(2f);
        Global.currentLevel++;
        SceneManager.LoadSceneAsync(1);
    }
}
