﻿using System.Collections;
using UnityEngine;

public class Ship_Controller : MonoBehaviour
{
    public float movementSpeed = 1;

    public Ship_UI shipUI = null;

    private float movementX = 0.0f;
    private float movementY = 0.0f;

    public Ship_Stats stats = null;

    private void Awake()
    {
        if (Global.ship != null)
        {
            Destroy(gameObject);
            return;
        }           

        Global.ship = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Global.playerCanMove)
        {
            GetInput();
            GetRotation();

            transform.position += new Vector3(movementX, movementY, 0) * movementSpeed * Time.deltaTime;
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -Global.screenWidth, Global.screenWidth),
                                             Mathf.Clamp(transform.position.y, -Global.screenHeight, Global.screenHeight), 0.0f);
        }
    }

    private void GetRotation()
    {
        Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        Vector3 positionToRotateTo = mouseWorld - transform.position;
        float rotationZ = -Mathf.Atan2(positionToRotateTo.x, positionToRotateTo.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);        
    }

    private void GetInput() 
    {
        movementX = Input.GetAxis("Horizontal");
        movementY = Input.GetAxis("Vertical");
    }

    public IEnumerator PlayerArriveAtLevel() 
    {
        Global.sounds.EndSpaceJump();
        yield return new WaitForSeconds(0.3f);

        float counter = 0;

        transform.position = new Vector3(0, -Global.screenHeight - 40, 1);

        Vector3 start = transform.position;
        Vector3 end = new Vector3(0, 0, 1);

        transform.localScale = new Vector3(3.5f, 7f, 1);

        while (counter < 1) 
        {
            counter += Time.deltaTime * 2;
            transform.position = Vector3.Lerp(start, end, counter);
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.7f, 0.7f, 0.7f), counter);
            yield return null;
        }
    }

    public IEnumerator PlayerLeaveLevel()
    {
        Global.sounds.BeginSpaceJump();
        yield return new WaitForSeconds(0.3f);

        float counter = 0;

        Vector3 start = transform.position;
        Vector3 end = new Vector3(0, Global.screenHeight + 40, 1);

        transform.rotation = Quaternion.identity;

        while (counter < 1)
        {
            counter += Time.deltaTime * 2;
            transform.position = Vector3.Lerp(start, end, counter);
            yield return null;
        }
    }
}
