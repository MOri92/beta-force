﻿using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class Ship_Explode : MonoBehaviour
{
    [SerializeField] private GameObject explosion = null;
    [SerializeField] private VisualEffect visual = null;

    private string attraction = "Attraction Force";
    private string spawn = "Spawn";

    private int maxForce = 300, minForce = 10;

    public void ExplodeShip() 
    {
        explosion.SetActive(true);
        StartCoroutine(Exploding());
    }

    private IEnumerator Exploding() 
    {
        //yield return new WaitForSeconds(0.5f);
        visual.SetFloat(attraction, maxForce);
        yield return new WaitForSeconds(0.5f);
        visual.SetFloat(spawn, 0);
        visual.SetFloat(attraction, minForce);
        yield return new WaitForSeconds(4.0f);
        explosion.SetActive(false);
    }
}
