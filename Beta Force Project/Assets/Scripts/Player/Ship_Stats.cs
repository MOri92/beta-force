﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ship_Stats : MonoBehaviour
{
    public int maxHitPoints = 10;
    public int hitPoints = 10;

    private int shieldDuration = 10;
    public float shieldEnergy = 1.0f;

    public int bulletStrenghtModifier = 1;
    public float bulletSpeedModifier = 0.2f;
    public float cadencyModifier = 1.0f;

    [SerializeField] private float reactionTime = 1.0f;
    [SerializeField] private GameObject shield = null;

    private Animator anim = null;
    private Coroutine incomingImpact = null;
    private Ship_Controller controller = null;

    private void Start()
    {
        Global.stats = this;

        anim = GetComponent<Animator>();
        controller = GetComponent<Ship_Controller>();
    }

    public void ReceiveDamage(int damage)
    {
        if (!shield.activeSelf)
        {
            hitPoints -= damage;
            if (hitPoints <= 0)
            {
                gameObject.SetActive(false);
                GameOver();
            }
            controller.shipUI.UpdateLife();
        }
    }

    public void Heal(int healEffect)
    {
        hitPoints += healEffect;
        if (hitPoints > maxHitPoints) hitPoints = maxHitPoints;

        controller.shipUI.UpdateLife();
    }

    public void RaiseHealth() 
    {
        maxHitPoints += 10;
    }

    public void RaiseShieldDuration() 
    {
        shieldDuration += 2;
    }

    public void RaiseShieldCharge(float charge)
    {
        if (shieldEnergy < 1)
        {
            shieldEnergy += charge;
            controller.shipUI.GotShieldCharge(charge);
            if (shieldEnergy > 1) shieldEnergy = 1;
        }
    }

    public void ReduceBulletCadency()
    {
        cadencyModifier -= 0.1f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy_Projectile"))
        {
            if (incomingImpact == null) 
                incomingImpact = StartCoroutine(ImpactIncoming(collision.gameObject));
        }
    }
    private IEnumerator ImpactIncoming(GameObject incomingBullet) 
    {
        float counter = 0;
        anim.SetBool("Alert", true);
        Time.timeScale = 0.2f;

        while(counter < 1) 
        {
            counter += Time.unscaledDeltaTime * reactionTime;

            if (Input.GetMouseButtonDown(1)) 
            {
                StopCoroutine(incomingImpact);                
                DodgeBullet();
            }

            yield return null;
        }

        Time.timeScale = 1f;
        Global.sounds.PlayerHitted();
        anim.SetBool("Alert", false);
        Bullet bullet = incomingBullet.GetComponent<Bullet>();
        int damage = bullet.actualDamage;
        bullet.ResetProjectile();
        ReceiveDamage(damage);
        incomingImpact = null;
    }

    private void DodgeBullet() 
    {
        Global.sounds.PlayerDodge();
        anim.SetTrigger("Dodge");
        anim.SetBool("Alert", false);
        Time.timeScale = 1f;
        incomingImpact = null;
    }

    public IEnumerator ShieldUp()
    {
        Global.ship.shipUI.GotShieldCharge(0);
        shieldEnergy = 0;
        shield.SetActive(true);
        yield return new WaitForSeconds(shieldDuration);
        shield.SetActive(false);
    }

    private void GameOver() 
    {
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }
}
