﻿using UnityEngine;

public class Ship_Shooter : MonoBehaviour
{
    [SerializeField] private Ship_Cannon[] cannon = new Ship_Cannon[1];

    private float fireCounter = 0;

    void Update()
    {
        if (Global.playerCanMove) 
        {
            GetInputs();
            RechargeCannons();
        }
    }

    private void GetInputs()
    {
        if(Input.GetMouseButton(0) && fireCounter == 0) 
        {
            FireCannons();
        }

        if(Global.stats.shieldEnergy == 1 && Input.GetKeyDown(KeyCode.Alpha1) && Global.playerGotShield) 
        {
            ActivateShield();
        }
    }

    private void ActivateShield()
    {
        StartCoroutine(Global.stats.ShieldUp());
        Global.sounds.ShieldUp();
    }

    private void RechargeCannons()
    {
        if      (fireCounter > 0) fireCounter -= Time.deltaTime;
        else if (fireCounter < 0) fireCounter  = 0;
    }

    private void FireCannons()
    {
        foreach (Ship_Cannon toShoot in cannon)
        {
            toShoot.ShootProjectile(Global.stats.bulletStrenghtModifier, Global.stats.bulletSpeedModifier);
        }

        Global.sounds.PlayerShoots();
        fireCounter = Global.stats.cadencyModifier;
    }
}
