﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Ship_UI : MonoBehaviour
{
    private TMP_Text life = null;
    private TMP_Text gold = null;

    private Image shieldIcon = null;

    private const string lifeText = "Life: ";

    public void GetUIElements(TMP_Text lifeText, TMP_Text goldText, Image shield) 
    {
        life = lifeText;
        gold = goldText;
        shieldIcon = shield;

        gold.text = Global.money.ToString();
        GotShieldCharge(1);
        UpdateLife();
    }

    public void UpdateLife()
    {
        life.text = lifeText + Global.stats.hitPoints + "/" + Global.stats.maxHitPoints;
    }

    public void GotMoney(int moneyReceived) 
    {
        StartCoroutine(ReceiveMoney(moneyReceived));
    }

    public void GotShield() 
    {
        shieldIcon.transform.parent.gameObject.SetActive(true);
    }

    public void GotShieldCharge(float charge) 
    {
        StartCoroutine(ReceiveShieldCharge(charge));
    }

    private IEnumerator ReceiveMoney(int moneyReceived) 
    {
        float counter = 0;

        int startingMoney = Global.money;
        int endMoney = Global.money + moneyReceived;

        while (counter < 1)
        {
            counter += Time.deltaTime * 3;
            Global.money = Mathf.RoundToInt(Mathf.Lerp(startingMoney, endMoney, counter));
            gold.text = Global.money.ToString();
            yield return null;
        }
    }

    private IEnumerator ReceiveShieldCharge(float chargeReceived)
    {
        float counter = 0;

        float startingCharge = shieldIcon.fillAmount;
        float endCharge = Mathf.Clamp(startingCharge + chargeReceived, 0, 1);

        while (counter < 1)
        {
            counter += Time.deltaTime * 3;
            shieldIcon.fillAmount = Mathf.Lerp(startingCharge, endCharge, counter);
            yield return null;
        }
    }
}
