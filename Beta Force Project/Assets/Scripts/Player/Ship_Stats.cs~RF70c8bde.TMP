﻿using System.Collections;
using UnityEngine;

public class Ship_Stats : MonoBehaviour
{
    private int maxHitPoints = 10;
    private int hitPoints = 10;

    private int shieldDuration = 10;
    public float shieldEnergy = 1.0f;

    public int bulletStrenghtModifier = 1;
    public float bulletSpeedModifier = 0.2f;
    public float cadencyModifier = 1.0f;

    [SerializeField] private float reactionTime = 1.0f;
    [SerializeField] private GameObject dodgeButton = null;
    [SerializeField] private GameObject shield = null;

    private Animator anim = null;
    private Coroutine incomingImpact = null;
    private Ship_Controller controller = null;

    private void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<Ship_Controller>();
        controller.UpdateUI(hitPoints, maxHitPoints);
    }

    public void ReceiveDamage(int damage)
    {
        if (!shield.activeSelf)
        {
            hitPoints -= damage;
            if (hitPoints <= 0) DestroyShip();

            controller.UpdateUI(hitPoints, maxHitPoints);
        }
    }

    public void Heal(int healEffect)
    {
        hitPoints += healEffect;
        if (hitPoints > maxHitPoints) hitPoints = maxHitPoints;

        controller.UpdateUI(hitPoints, maxHitPoints);
    }

    public void RaiseHealth() 
    {
        maxHitPoints += 10;
    }

    public void RaiseShield() 
    {
        shieldDuration += 2;
    }

    public void ReduceBulletCadency()
    {
        cadencyModifier -= 0.1f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy_Projectile"))
        {
            if (incomingImpact == null) 
                incomingImpact = StartCoroutine(ImpactIncoming(collision.gameObject));
        }
    }
    private IEnumerator ImpactIncoming(GameObject incomingBullet) 
    {
        float counter = 0;
        anim.SetBool("Alert", true);

        while(counter < 1) 
        {
            counter += Time.unscaledDeltaTime * reactionTime;

            if (Input.GetMouseButtonDown(1)) 
            {
                StopCoroutine(incomingImpact);
                incomingImpact = null;
                StartCoroutine(DodgeBullet());
            }

            yield return null;
        }

        anim.SetBool("Alert", false);
        Bullet bullet = incomingBullet.GetComponent<Bullet>();
        int damage = bullet.actualDamage;
        bullet.ResetProjectile();
        ReceiveDamage(damage);
        incomingImpact = null;
    }

    private IEnumerator DodgeBullet() 
    {
        anim.SetTrigger("Dodge");
        anim.SetBool("Alert", false);
        Debug.Log("Bullet Dodged");
        yield return null;
    }

    private void DestroyShip()
    {
        Destroy(gameObject);
    }

    public IEnumerator ShieldUp()
    {
        shieldEnergy = 0;
        shield.SetActive(true);
        yield return new WaitForSeconds(shieldDuration);
        shield.SetActive(false);
    }
}
