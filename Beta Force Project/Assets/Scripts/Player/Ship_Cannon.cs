﻿using System.Collections.Generic;
using UnityEngine;

public class Ship_Cannon : MonoBehaviour
{
    public List<Bullet> projectiles = new List<Bullet>();

    public void ShootProjectile(int damageMod, float speedMod) 
    {
        projectiles[0].gameObject.SetActive(true);
        projectiles[0].Shot(damageMod, speedMod);
        projectiles.RemoveAt(0);
    }
}
