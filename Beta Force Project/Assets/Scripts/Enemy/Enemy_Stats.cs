﻿using UnityEngine;

public class Enemy_Stats : MonoBehaviour
{
    [SerializeField] private int hitPoints = 10;
    private Enemy_Squad squad = null;
    private Enemy_Controller controller = null;
    
    private void Start()
    {
        squad = transform.parent.GetComponent<Enemy_Squad>();
        controller = GetComponent<Enemy_Controller>();
    }

    public void ReceiveDamage(int damage)
    {
        hitPoints -= damage;
        if (hitPoints <= 0) DestroyShip();
    }

    private void DestroyShip() 
    {
        Global.sounds.ShipDestroyed();
        controller.DropSpoils();
        squad.ShipDestroyed();
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            int damage = bullet.actualDamage;
            bullet.ResetProjectile();
            ReceiveDamage(damage);
        }
    }    
}
