﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy_Formation : MonoBehaviour
{
    [SerializeField] protected List<Enemy_Controller> enemies = new List<Enemy_Controller>();

    protected float currentlyAlive = 0;
    protected Level_Controller currentLevel = null;

    public void StartFormation(Level_Controller level, List<Enemy_Controller> enemiesInSquad, float timeUntilDeployment) 
    {
        currentLevel = level;
        enemies.AddRange(enemiesInSquad);
        currentlyAlive = enemiesInSquad.Count;
        StartCoroutine(WaitForDeployment(timeUntilDeployment));
    }

    protected IEnumerator WaitForDeployment(float timeUntilDeployment) 
    {
        yield return new WaitForSeconds(timeUntilDeployment);
        SpawnSquad();
    }

    protected virtual void SpawnSquad() 
    {

    }    
}
