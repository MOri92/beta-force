﻿using UnityEngine;

public class Enemy_Shield : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile")) 
        {
            Debug.Log("hitted " + gameObject);
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            bullet.ResetProjectile();
        }
    }
}
