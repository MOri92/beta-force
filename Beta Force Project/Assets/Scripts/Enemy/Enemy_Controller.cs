﻿using System.Collections;
using UnityEngine;

public delegate void D_Void();

public abstract class Enemy_Controller : MonoBehaviour
{
    [SerializeField] protected float shipSpeed = 1.0f;

    protected GameObject player = null;
    protected Enemy_Attack attack = null;
    protected Enemy_Spoils spoils = null;

    protected void Start()
    {
        player = Global.ship.gameObject;
        spoils = GetComponent<Enemy_Spoils>();
    }

    protected virtual void Update()
    {
        Rotate();
    }

    public void EnableAttack(bool canAttack) 
    {
        if(attack == null) attack = GetComponent<Enemy_Attack>();
        attack.canAttack = canAttack;
    }

    public void DropSpoils()
    {
        spoils.OnShipDestroyed();
    }

    public void MoveTo(Vector3 endPoint, D_Void movementFinished) 
    {
        StartCoroutine(Moving(endPoint, movementFinished));
        EnableAttack(true);
    }

    protected void Rotate() 
    {        
        Vector3 positionToRotateTo = player.transform.position - transform.position;
        float rotationZ = -Mathf.Atan2(positionToRotateTo.x, positionToRotateTo.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
    }

    protected virtual IEnumerator Moving(Vector3 endPoint, D_Void movementFinished) 
    {
        float counter = 0;

        Vector3 startingPoint = transform.position;

        while(counter < 1) 
        {
            counter += Time.deltaTime * shipSpeed;
            transform.position = Vector3.Lerp(startingPoint, endPoint, counter);
            yield return null;
        }

        if (movementFinished != null) movementFinished();
    }

    protected void OnBecameVisible()
    {
        if (attack == null) attack = GetComponent<Enemy_Attack>();
        if (!attack.canAttack) attack.canAttack = true;
    }
}
