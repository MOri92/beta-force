﻿using System.Collections.Generic;
using UnityEngine;

public class Enemy_Squad : MonoBehaviour
{
    [SerializeField] private Enemy_Formation[] formations = new Enemy_Formation[1];

    private int currentFormation = 0;
    private int memberDestroyed = 0;

    private Level_Controller level = null;
    private List<Enemy_Controller> members = new List<Enemy_Controller>();

    private void Awake()
    {
        foreach (Transform ship in transform)
        {
            members.Add(ship.GetComponent<Enemy_Controller>());
        }    
    }

    public void DeploySquad(Level_Controller levelController, float deployTimer) 
    {
        level = levelController;

        for (int i = 0; i < members.Count; i++)
        {
            members[i].transform.position = new Vector3(0, 30, 0);
            members[i].EnableAttack(false);
            members[i].gameObject.SetActive(true);
        }

        formations[currentFormation].StartFormation(level, members, deployTimer);
    }

    public void ShipDestroyed() 
    {
        memberDestroyed++;

        if (memberDestroyed == members.Count) SquadDestroyed(); 
    }

    private void SquadDestroyed() 
    {
        level.isWaveOver();
    }
}
