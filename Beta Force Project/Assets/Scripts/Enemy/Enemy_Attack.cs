﻿using UnityEngine;

public class Enemy_Attack : MonoBehaviour
{
    public bool canAttack = false;

    [SerializeField] private int level = 1;
    
    [SerializeField] private Ship_Cannon[] cannon = new Ship_Cannon[1];
    
    private float fireCounterMax = 2f;
    private float fireCounter = 0;

    private void Update()
    {
        if(canAttack) RechargeCannons();
    }

    private void RechargeCannons()
    {
        if (fireCounter >= 0) fireCounter -= Time.deltaTime;
        else if (fireCounter < 0) FireCannons();
    }

    private void FireCannons()
    {
        foreach (Ship_Cannon toShoot in cannon)
        {
            toShoot.ShootProjectile(level, level + level/10);
        }

        Global.sounds.EnemyShoots();
        fireCounter = fireCounterMax;
    }
}
