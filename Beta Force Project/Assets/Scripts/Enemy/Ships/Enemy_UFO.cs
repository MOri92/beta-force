﻿using System.Collections;
using UnityEngine;

public class Enemy_UFO : Enemy_Controller
{
    // Update overrided because UFO doesn't rotate towards player.
    protected override void Update()
    {
    }

    // Moves the UFO around
    protected override IEnumerator Moving(Vector3 endPoint, D_Void movementFinished)
    {
        float counter = 0;

        Vector3 startingPoint = transform.position;
        Vector3 endingPoint = new Vector3(Random.Range(-Global.screenWidth, Global.screenWidth),
                                          Random.Range(-Global.screenHeight, Global.screenHeight), 1);

        while (counter < 1)
        {
            counter += Time.deltaTime * shipSpeed;
            transform.position = Vector3.Lerp(startingPoint, endingPoint, counter);
            yield return null;
        }

        StartCoroutine(WaitForNewLocation());
    }

    // Waits some time in the new position
    private IEnumerator WaitForNewLocation() 
    {
        yield return new WaitForSeconds(Random.Range(1, 2));
        StartCoroutine(Moving(Vector3.zero, null));
    }
}
