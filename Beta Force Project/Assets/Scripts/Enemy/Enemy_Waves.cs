﻿using System;
using System.Collections.Generic;

[Serializable]
public class Enemy_Waves
{
    public Enemy_SubWave[] subWaves = new Enemy_SubWave[3];
}

[Serializable]
public class Enemy_SubWave
{
    public float[] deployTime = new float[3];
    public List<Enemy_Squad> enemySquads = new List<Enemy_Squad>();
}
