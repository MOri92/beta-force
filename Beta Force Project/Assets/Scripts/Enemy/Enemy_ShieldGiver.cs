﻿using UnityEngine;

public class Enemy_ShieldGiver : MonoBehaviour
{
    [SerializeField] private GameObject shield = null;
    [SerializeField] private Rigidbody2D shipCollider = null;

    private int hitPoints = 4;

    private void Update()
    {
        transform.RotateAround(shipCollider.transform.position, transform.forward, 0.05f);
    }
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            hitPoints -= bullet.actualDamage;
            bullet.ResetProjectile();

            if (hitPoints <= 0)
            {
                shield.SetActive(false);
                gameObject.SetActive(false);
                shipCollider.simulated = true;
            }
        }
    }
}
