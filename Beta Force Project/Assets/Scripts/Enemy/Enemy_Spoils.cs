﻿using System.Collections;
using UnityEngine;

public class Enemy_Spoils : MonoBehaviour
{
    [SerializeField] private Drops[] drops = new Drops[2];

    public void OnShipDestroyed() 
    {
        ReleaseDrops();
    }

    private void ReleaseDrops() 
    {
        if (drops.Length > 0) 
        {
            foreach (Drops item in drops)
            {                
                item.Fall();
            }
        }
    }
}
