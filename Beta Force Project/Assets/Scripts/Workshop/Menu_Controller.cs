﻿using TMPro;
using UnityEngine;

public class Menu_Controller : MonoBehaviour
{
    [SerializeField] private GameObject armorPanel = null;
    [SerializeField] private GameObject firePanel = null;

    [SerializeField] private TMP_Text trees = null;
    [SerializeField] private Dialog_System_Controller dialog = null;

    private void Start()
    {
        UpdateTreeSamples();
    }

    public void EnablePanel(int id)
    {
        armorPanel.SetActive(false);
        firePanel.SetActive(false);

        switch (id)
        {
            case 0:
                armorPanel.SetActive(true);
                break;

            case 1:
                firePanel.SetActive(true);
                break;
        }
    }

    public void ItemPurchased(int item)
    {
        if (Global.money >= 10)
        {
            if (Global.shipLevels[item - 1] < 10)
            {
                switch (item)
                {
                    case 1:
                        Global.stats.RaiseHealth();
                        break;

                    case 2:
                        Global.stats.RaiseShieldDuration();
                        break;

                    case 3:
                        Global.ship.movementSpeed += 1f;
                        break;

                    case 4:
                        Global.stats.bulletStrenghtModifier++;
                        break;

                    case 5:
                        Global.stats.bulletSpeedModifier += 0.5f;
                        break;

                    case 6:
                        Global.stats.ReduceBulletCadency();
                        break;
                }

                Global.shipLevels[item - 1]++;
                Global.money -= 10;
                dialog.NewDialog(5, null);
                UpdateTreeSamples();
            }
            else
            {
                dialog.NewDialog(7, null);
            }
        }
        else
        {
            dialog.NewDialog(9, null);
        }
    }

    private void UpdateTreeSamples() 
    {
        trees.text = Global.money.ToString();
    }
}